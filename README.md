# Setup WebdriverIO
## Requirements
### Node
- Node 16.14.0
  Run this command to configure your webdriver

  ```
  npx wdio config
  ```
### Android
- Android Studio
  ```
  Make sure that ANDROID_HOME & JAVA_HOME set properly check with `appium-doctor`
  ```
### iOS
- Xcode
---
## Init Project
initialize a project and create the package.json file
```
# Step 1

  npm init -y
  npm init wdio .

  or

  npm init -y
  npm init wdio . -- --yes

Step 2

  npm install -g appium@next
  npm install -g appium-doctor

Step 3

  # Android
  appium driver install uiautomator2

  # iOS
  appium driver install xcuitest

Step 4

    npm install @wdio/appium-service --save-dev
```
---
## Run Project
```
source ~/.bash_profile

```
npm run test:android

or 

npm run test:ios
```