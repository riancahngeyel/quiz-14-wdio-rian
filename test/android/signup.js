const assert = require('assert');
describe('Native Demo App', () => {
    it('Sign Up', async () => {
        // click login button
        const loginButton = await $("~Login");
        await loginButton.click();
        // click sign up
        const signUpTab = await $("~button-sign-up-container");
        await signUpTab.click();
  
        // generate email random
        const randomString = Math.random().toString(36).substring(2, 8);
  
        // email combination
        const randomEmail = `testing${randomString}@mailinator.com`;
  
        //input email
        const inputEmail = await $("~input-email");
        await inputEmail.setValue(randomEmail);
        await driver.pause(1000);

        //input password
        const inputPass = await $("~input-password");
        await inputPass.setValue("12345678");
        await driver.pause(1000);

        //repeat password
        const inputConfirmPass = await $("~input-repeat-password");
        await inputConfirmPass.setValue("12345678");
        await driver.pause(1000);
  
        // click sign up
        const signUpButton = await $("~button-SIGN UP");
        await signUpButton.click();
        await driver.pause(2500);

        //validation pop up
        const message = await driver.$('android=new UiSelector().resourceId("android:id/message")');
        const messageText = await message.getText();
        assert.strictEqual(messageText, 'You successfully signed up!');

        //click ok
        const OK = await $('android=new UiSelector().resourceId("android:id/button1")');
        await OK.click();
        await driver.pause(2500);
      });
});